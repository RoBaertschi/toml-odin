package toml

import "core:c"
import "core:c/libc"

when ODIN_OS == .Linux || ODIN_OS == .Darwin {
	foreign import lib "libtoml.a"
} else when ODIN_OS == .Windows {
	foreign import lib "libtoml.a"
}

table :: struct {}
array :: struct {}

timestamp :: struct {
	__buffer: struct {
		year, month, day: c.int,
		hour, minute, second, millisec: c.int,
		z: [10]c.char,
	},
	year, month, day: ^c.int,
	hour, minute, second, millisec: ^c.int,
	z: ^c.char,
}

datum :: struct {
	ok: c.int,
	u: struct #raw_union {
		ts: ^timestamp,
		s: cstring,
		b: c.int,
		i: c.int64_t,
		d: c.double,
	},
}

@(default_calling_convention = "c", link_prefix="toml_")
foreign lib {
	parse_file :: proc(fp: ^libc.FILE, errbuf: cstring, errbufsz: c.int) -> ^table ---
	parse :: proc(conf, errbuf: cstring, errbufsz: c.int) -> ^table ---
	free :: proc(tab: ^table) ---

	array_nelem :: proc(arr: ^array) -> c.int ---
	string_at :: proc(arr: ^array, idx: c.int) -> datum ---
	bool_at :: proc(arr: ^array, idx: c.int) -> datum ---
	int_at :: proc(arr: ^array, idx: c.int) -> datum ---
	double_at :: proc(arr: ^array, idx: c.int) -> datum ---
	timestamp_at :: proc(arr: ^array, idx: c.int) -> datum ---
	array_at :: proc(arr: ^array, idx: c.int) -> ^array ---
	table_at :: proc(arr: ^array, idx: c.int) -> ^table ---

	key_in :: proc(tab: ^table, keyidx: c.int) -> cstring ---
	key_exists :: proc(tab: ^table, key: cstring) -> c.int ---
	
	string_in :: proc(tab: ^table, key: cstring) -> datum ---
	bool_in :: proc(tab: ^table, key: cstring) -> datum ---
	int_in :: proc(tab: ^table, key: cstring) -> datum ---
	double_in :: proc(tab: ^table, key: cstring) -> datum ---
	timestamp_in :: proc(tab: ^table, key: cstring) -> datum ---
	array_in :: proc(tab: ^table, key: cstring) -> ^array ---
	table_in :: proc(tab: ^table, key: cstring) -> ^table ---
	
	array_kind :: proc(arr: ^array) -> c.char ---
	array_type :: proc(arr: ^array) -> c.char ---
	array_key :: proc(arr: ^array) -> cstring ---
	table_nkval :: proc(tab: ^table) -> c.int ---
	table_narr :: proc(tab: ^table) -> c.int ---
	table_ntab :: proc(tab: ^table) -> c.int ---
	table_key :: proc(tab: ^table) -> cstring ---
	
	utf8_to_ucs :: proc(orig: cstring, len: c.int, ret: ^c.int64_t) -> c.int ---
	ucs_to_utf8 :: proc(code: c.int64_t, buf: [6]c.char) -> c.int ---
	set_memutil :: proc(xxmalloc: proc "c" (c.size_t) -> rawptr, xxfree: proc "c" (rawptr)) ---
}
